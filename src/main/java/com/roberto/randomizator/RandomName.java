package com.roberto.randomizator;

import java.io.*;
import java.util.*;

public class RandomName {

    //function buat update file yang udah ada
    public static String updateFile(ArrayList<String> arrList) throws IOException {
        try (FileWriter writer = new FileWriter("file.txt")) {
            for (String str : arrList) {
                writer.write(str + System.lineSeparator());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return "success";
    }

    public static void main(String[] args) throws IOException {

        BufferedReader bufReader = new BufferedReader(new FileReader("file.txt"));
        ArrayList<String> listOfNames = new ArrayList<>();
        Random rand = new Random();
        String line = bufReader.readLine();
        while (line != null) {
            listOfNames.add(line);
            line = bufReader.readLine();
        }
        bufReader.close();
        int index = rand.nextInt(listOfNames.size());
        String selectedName = listOfNames.get(index);
        System.out.println("Congrats " + selectedName.toUpperCase() + "!!");
        listOfNames.remove(index);

        updateFile(listOfNames);
    }
}
